SECRET_KEY = 'secret'


INSTALLED_APPS = (
    # Django Apps
    'django.contrib.auth',
    'django.contrib.contenttypes',

    # Third Party Apps
    'django_nose',

    # Custom Apps
    'todo_api',
)


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}


# Configure django nose as the test runner
TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'


# Root URL configuration
ROOT_URLCONF = 'test_urls'
