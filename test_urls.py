from django.conf.urls import include, url


urlpatterns = [
    url(r'^', include('todo_api.urls')),
]
